<?php 
/* 3- Un entero positivo se llama perfecto si éste es igual a la suma de todos sus divisores
diferentes de él. Por ejemplo:
6 es perfecto porque 6 = 1 + 2 + 3.
28 es perfecto porque 28 = 1 + 2 + 4 + 7 + 14

Escriba un programa que reciba como entrada n números enteros positivos, y por cada uno
de ellos imprima sus divisores e indique si es perfecto o no. */

<?php

function Punto3($numeros = []){

    foreach ($numeros as $value) {
    
        if((int)$value > 0 && is_numeric($value)){
            
            echo "Divisoes de ".$value." : ";
            $suma = 0 ;
            for($i = 1; $i < $value ; $i++){
                
                if(( $value % $i ) == 0){
                    
                    echo $i.", ";
                    $suma = $suma + $i;
                }
            }
            
            if($suma == $value){
                echo "<b>si</b> es positivo perfecto\n";
            }else{
                 echo "<b>no</b> es positivo perfecto\n";
            }
            
        }else{
            echo "\nEl valor ingresado '".$value."' no es valido\n";
        }
    }
}

$numeros = [36,2,42,7,6,9,10,36,28,45,100,"x",-7];
Punto3($numeros);

?>


 ?>