<?php
/*4- Escriba un algoritmo que imprima los números del 1 al 100. Pero para los múltiplos
de 3 imprima "Fizz" en lugar del número y para los múltiplos de 5 imprima "Buzz". Para los
números que son múltiplos de ambos imprima "FizzBuzz".*/


function punto4(){

	for( $i = 1; $i < 101; $i++ ){
		if( $i % (5*3) === 0 ){
			echo "FizzBuzz\n";
		}elseif( $i % 3 === 0 ){
			echo "Fizz\n";
		}elseif( $i% 5 === 0 ){
			echo "Buzz\n";
		}else{
			echo $i ."\n";
		}
	}
}

punto4();

?>