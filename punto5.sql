CREATE DATABASE mascotasymascotas;

CREATE TABLE tipo_mascota(
   idtipo_mascota int not  null AUTO_INCREMENT,
   descripcion varchar(100) not null,
   PRIMARY KEY (idtipo_mascota)
 )ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE estado_mascota(
   idestado_mascota int not  null AUTO_INCREMENT,
   estado varchar(20) not null,
   PRIMARY KEY (idestado_mascota)
 )ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
 
 CREATE TABLE propietario(
   idpropietario INT,
   nombre varchar(50),
   direeccion varchar(100),
   telefono varchar(10),
   correo varchar(50),
   comentario varchar(200),
   PRIMARY KEY (idpropietario)
 )ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
 CREATE TABLE mascota(
   idmascota int not null AUTO_INCREMENT,
   nombre varchar(50),
   tipo_mascota INT,
   propietario INT,
   estado_mascota INT,
    PRIMARY KEY(idmascota),
    FOREIGN KEY (tipo_mascota)
    	REFERENCES tipo_mascota(idtipo_mascota),
   	FOREIGN KEY (propietario)
    	REFERENCES propietario (idpropietario),
    FOREIGN KEY (estado_mascota)
    	REFERENCES estado_mascota (idestado_mascota)
 )ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO tipo_mascota (descripcion) 
VALUES ('Perro'),('Gato'),('Pajaro'),('Dinosaurio'),('Conejo');

INSERT INTO estado_mascota (estado) VALUES ('Adoptado'),('No adoptado');


INSERT INTO propietario (idpropietario, nombre, direeccion, telefono, correo, comentario) 
VALUES ('0', 'No asignado', NULL, NULL, NULL, 'Se le asigna a las mas costas que aun no tienen propietario');

INSERT INTO propietario (idpropietario, nombre, direeccion, telefono, correo, comentario) 
VALUES ('1', 'Luis David Montes', 'Mi direccion', '3105338029', 'mi correo', 'es un excelente desarrollador');

INSERT INTO propietario (idpropietario, nombre, direeccion, telefono, correo, comentario) 
VALUES ('2', 'Valentina Rodirguez', 'Mi direccion', '3125020064', 'mi correo', 'una buena protectora');

INSERT INTO propietario (idpropietario, nombre, direeccion, telefono, correo, comentario) 
VALUES ('3', 'Maria Gomez', 'Mi direccion', '3105338029', 'mi correo', 'una buena protectora');

INSERT INTO propietario (idpropietario, nombre, direeccion, telefono, correo, comentario)
VALUES ('4', 'Julian Rios', 'mi direcccion', '33333333', 'mi correo', 'no se recomienda');

INSERT INTO mascota (idmascota, nombre, tipo_mascota, propietario, estado_mascota)
 VALUES (NULL, 'Michin', '4', '1', '1'),
 (NULL, 'Lulu', '6', '2', '1'),
 (NULL, 'Simon', '3', '0', '2'), 
 (NULL, 'Mateo', '3', '3', '1'),
 (NULL, 'Pepito', '5', '1', '1'),
 (NULL, 'Killer', '6', '3', '1'),
 (NULL, 'Anastacio', '5', '2', '1'),
 (NULL, 'kiko', '6', '0', '2'),
  (NULL, 'Juana', '4', '1', '1');


--● Listar todas las mascotas.
  SELECT * FROM mascota
--● Listar las mascotas que no han sido adoptadas.
  SELECT * FROM mascota where estado_mascota = 2 OR propietario = 0;
-- ● Listar el número de mascotas por cada tipo de mascota.
  SELECT count(1) as numero_mascota,  tm.descripcion as tipo_mascota FROM mascota m
    JOIN tipo_mascota  tm ON tm.idtipo_mascota =  tipo_mascota GROUP BY idtipo_mascota
-- ● Listar los propietarios que tengan más de una mascota.
  SELECT p.nombre as propietario, count(1) as cant_mascotas FROM mascota m
    JOIN propietario p ON  p.idpropietario = m.propietario GROUP by p.idpropietario HAVING cant_mascotas > 1;
-- ● Listar el número de mascotas por cada tipo de mascota y por cada propietario.
  SELECT p.nombre as propietario, tm.descripcion as tipo_mascota,count(1) as cant_mascotas FROM mascota m
    JOIN propietario p ON  p.idpropietario = m.propietario
    JOIN tipo_mascota  tm ON tm.idtipo_mascota =  tipo_mascota 
    GROUP by p.idpropietario,tm.idtipo_mascota ;
--● Listas los propietarios que no tienen mascotas.
  SELECT * FROM propietario where idpropietario not in (select propietario from mascota);