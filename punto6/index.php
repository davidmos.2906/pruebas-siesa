<?php
/* Indicar la siguiente url http://localhost:90/siesa/punto6/index.php para hacer el consumo tipo GET desde un cliente "POSTMAN"

NOTA: El puerto vaira segun su localhost o srvidor, en mi caso yo use el 90 pero normalmente es el 80*/

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
    $conexion = mysqli_connect('localhost','root','','mascotasymascotas'); 
	mysqli_set_charset($conexion, "utf8");

	$sql="SELECT * FROM mascota";

	$resultado = mysqli_query($conexion,$sql);

	$data = [];

	while($fila = mysqli_fetch_array($resultado)){

		$data[]['idmascota']  = $fila['idmascota'];
		$data[]['nombre']  = $fila['nombre'];
		$data[]['tipo_mascota']  = $fila['tipo_mascota'];
		$data[]['propietario']  = $fila['propietario'];
		$data[]['estado_mascota']  = $fila['estado_mascota'];
	}

	echo json_encode($data);
	
	exit();
}

header("HTTP/1.1 400 Bad Request");

?>